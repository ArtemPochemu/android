package com.example.user.controller;


import android.graphics.Color;
import android.graphics.drawable.shapes.Shape;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pes.androidmaterialcolorpickerdialog.ColorPicker;
import com.pes.androidmaterialcolorpickerdialog.ColorPickerCallback;
import com.ramotion.fluidslider.FluidSlider;
import com.rm.rmswitch.RMTristateSwitch;
import com.skydoves.colorpickerpreference.ColorEnvelope;
import com.skydoves.colorpickerpreference.ColorListener;
import com.skydoves.colorpickerpreference.ColorPickerView;

import kotlin.Unit;
import kotlin.jvm.functions.Function0;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment1 extends Fragment {

    MainActivity mainActivity = new MainActivity();
    RMTristateSwitch mTristateSwitch;
    ColorPicker cp = null; //Экземпляр ColorPickerDialog
    FluidSlider slider;

    public Fragment1() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment1, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){ //Иницициализация элементов во фрагменте

        final int max = 100; //Значения fluid_slider
        final int min = 0;
        final int total = max - min;



        TextView textView = getView().findViewById(R.id.textView);

        ColorPickerView colorPickerView = getView().findViewById(R.id.colorPickerView);
        ImageButton button_picker = (ImageButton) getView().findViewById(R.id.button);

        mTristateSwitch = (RMTristateSwitch) getView().findViewById(R.id.gradient_colors_switch);



        /****СЛАЙДЕР ЯРКОСТИ ЛЕНТЫ****/
        slider = getView().findViewById(R.id.fluidSlider);
        slider.setBeginTrackingListener(new Function0<Unit>() {
            @Override
            public Unit invoke() {



                textView.setVisibility(View.INVISIBLE);
                return Unit.INSTANCE;
            }
        });

        slider.setEndTrackingListener(new Function0<Unit>() {
            @Override
            public Unit invoke() {
                textView.setVisibility(View.VISIBLE);
                return Unit.INSTANCE;
            }
        });

        // Java 8 lambda
        slider.setPositionListener(pos -> { //Слушатель слайдера (FluidSlider)
            final int value =  (int)(min + total * pos);
            slider.setBubbleText(String.valueOf(value));
            mainActivity.led_brightness = mainActivity.find_percent(value); //Присваиваем значение процента в переменную в пределах диапазона 0-255
            return Unit.INSTANCE;
        });

        slider.setPosition(0.10f);
        slider.setStartText(String.valueOf(min)+"%");
        slider.setEndText(String.valueOf(max)+"%");
        /********/

        colorPickerView.setColorListener(new ColorListener() { //Слушатель цветовой палитры
            @Override
            public void onColorSelected(ColorEnvelope colorEnvelope) {
                int color[] = colorEnvelope.getColorRGB();
                color_entry(color[0], color[1], color[2]); //Записываем цвет
            }
        });


        button_picker.setOnClickListener(new View.OnClickListener() { //Слушатель кнопки открытия RGB диалога

            @Override
            public void onClick(View v) {
                open_color_dialog();
                cp.show();
                cp.setCallback(new ColorPickerCallback() { //Устнавливаем значение цветов через ColorPickerDialog в RGB формате
                    @Override
                    public void onColorChosen(@ColorInt int color) {
                        color_entry(Color.red(color), Color.green(color), Color.blue(color)); //Записываем цвет
                    }
                });


                cp.enableAutoClose(); // Enable auto-dismiss for the dialog
            }
        });

    }

    public void color_entry(int R, int G, int B){ //Эта функция записывает цвета в разные массивы в зависимости от положения RMTristateSwitch

       switch (mTristateSwitch.getState()){
           case RMTristateSwitch.STATE_LEFT:   //Основной цвет
               mainActivity.led_color_array[0] = R;
               mainActivity.led_color_array[1] = G;
               mainActivity.led_color_array[2] = B;
               //mTristateSwitch.setSwitchBkgLeftColor(Color.rgb(R, G, B));
               //mTristateSwitch.setSwitchToggleLeftColor(Color.rgb(R-35, G-37, B-40));
               break;
           case RMTristateSwitch.STATE_MIDDLE: //Цвет градиента 1
               mainActivity.led_color_array_gradient[0] = R;
               mainActivity.led_color_array_gradient[1] = G;
               mainActivity.led_color_array_gradient[2] = B;
               //mTristateSwitch.setSwitchBkgMiddleColor(Color.rgb(R, G, B));
               //mTristateSwitch.setSwitchToggleMiddleColor(Color.rgb(R-35, G-37, B-40));
               break;
           case RMTristateSwitch.STATE_RIGHT:  //Цвет градиента 2
               mainActivity.led_color_array_gradient[3] = R;
               mainActivity.led_color_array_gradient[4] = G;
               mainActivity.led_color_array_gradient[5] = B;
               //mTristateSwitch.setSwitchBkgRightColor(Color.rgb(R, G, B));
               //mTristateSwitch.setSwitchToggleRightColor(Color.rgb(R-35, G-37, B-40));
               break;
       }

    }

    public void open_color_dialog(){
        switch (mTristateSwitch.getState()) {
            case RMTristateSwitch.STATE_LEFT:   //Основной цвет
                cp = new ColorPicker(getActivity(), mainActivity.led_color_array[0], mainActivity.led_color_array[1], mainActivity.led_color_array[2]);
                break;
            case RMTristateSwitch.STATE_MIDDLE: //Цвет градиента 1
                cp = new ColorPicker(getActivity(), mainActivity.led_color_array_gradient[0], mainActivity.led_color_array_gradient[1],mainActivity.led_color_array_gradient[2]);
                break;
            case RMTristateSwitch.STATE_RIGHT:  //Цвет градиента 2
                cp = new ColorPicker(getActivity(), mainActivity.led_color_array_gradient[3], mainActivity.led_color_array_gradient[4],mainActivity.led_color_array_gradient[5]);
                break;
        }
    }

}
