package com.example.user.controller;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.ramotion.directselect.DSListView;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment2 extends Fragment {


    public Fragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) { //Иницициализация элементов во фрагменте

        // Prepare dataset
        List<AdvancedPOJO> exampleDataSet = AdvancedPOJO.getExampleDataset();

        // Create adapter with our dataset
        ArrayAdapter<AdvancedPOJO> adapter = new AdvancedAdapter(getContext(), R.layout.advanced_list_item, exampleDataSet);

        // Set adapter to our DSListView
        DSListView<AdvancedPOJO> pickerView = getView().findViewById(R.id.ds_country_picker);
        pickerView.setAdapter(adapter);

    }

}
