package com.example.user.controller;

import java.util.Arrays;
import java.util.List;

class AdvancedPOJO {

    private String title;
    private int icon;

    public AdvancedPOJO(String title, int icon) {
        this.title = title;
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public static List<AdvancedPOJO> getExampleDataset() {
        return Arrays.asList(
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp),
                new AdvancedPOJO("Тест", R.drawable.ic_notifications_black_24dp)
        );
    }
}
