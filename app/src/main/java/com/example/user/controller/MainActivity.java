package com.example.user.controller;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;


import com.irozon.justbar.BarItem;
import com.irozon.justbar.JustBar;
import com.irozon.justbar.interfaces.OnBarItemClickListener;
import com.ramotion.circlemenu.CircleMenuView;

import net.steamcrafted.lineartimepicker.dialog.LinearTimePickerDialog;

import java.util.Timer;
import java.util.TimerTask;

import io.alterac.blurkit.BlurLayout;


public class MainActivity extends AppCompatActivity {

    CircleMenuView menu;
    BlurLayout blurLayout;

    public int led_brightness = 255; //Для хранения диапазона значений (0-255) яркости диодов ленты

    public int led_color_array[] = {0,0,0}; //Для хранения диапазона значений (0-255) R(0) G(1) B(2) цветов диодов ленты
    public int led_color_array_gradient[] = {0,0,0,0,0,0}; //Для хранения диапазона значений (0-255) R(0) G(1) B(2) R(3) G(4) B(5) цветов диодов ленты для градиента

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        menu = (CircleMenuView) findViewById(R.id.circle_menu);
        blurLayout = (BlurLayout) findViewById(R.id.blurLayout);

        LinearTimePickerDialog dialog = LinearTimePickerDialog.Builder.with(this).setTextColor(Color.rgb(255,255,255))/*
        .setDialogBackgroundColor(R.color.colorPrimaryDark)
        .setPickerBackgroundColor(R.color.colorPrimary)
        .setLineColor(R.color.colorAccent)*/.build();
        dialog.show();

        JustBar justBar = (JustBar) findViewById(R.id.justbar);
        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, new Fragment1()).commit(); //Задаём фрагмент в Activity

        justBar.setOnBarItemClickListener(new OnBarItemClickListener() {
            @Override
            public void onBarItemClick(BarItem barItem, int position) {
                switch (position) {
                    case 0:
                            getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, new Fragment1()).commit(); //Задаём фрагмент в Activity
                        break;
                    case 1:
                        show_circle_menu();
                        break;
                    case 2:
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, new Fragment3()).commit();
                        break;
                }
            }
        });




        menu.setEventListener(new CircleMenuView.EventListener() {
            @Override
            public void onMenuOpenAnimationStart(@NonNull CircleMenuView view) {

            }

            @Override
            public void onMenuOpenAnimationEnd(@NonNull CircleMenuView view) {

            }

            @Override
            public void onMenuCloseAnimationStart(@NonNull CircleMenuView view) { //Выключаем blur и убираем меню
                hide_cirlce_menu();
            }

            @Override
            public void onMenuCloseAnimationEnd(@NonNull CircleMenuView view) {

            }

            @Override
            public void onButtonClickAnimationStart(@NonNull CircleMenuView view, int index) {

            }

            @Override
            public void onButtonClickAnimationEnd(@NonNull CircleMenuView view, int index) {
                switch (index){
                    case 0:
                        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                        startActivity(intent);
                        break;
                }
            }
        });



    }

    int range = 0;
    int percent_1 = 0;

    public int find_percent(int percent){ //Функция нахождения процента от числа 255
        range = 255*percent/100;
        return range;
    }

    public int percent(int num){
        percent_1 = 255*100/num;
        return percent_1;
    }

    public void show_circle_menu(){ //Показать меню с blur
        blurLayout.startBlur();
        blurLayout.setFPS(0);
        blurLayout.pauseBlur();
        blurLayout.setBlurRadius(19);
        blurLayout.setDownscaleFactor(1);
        blurLayout.setVisibility(View.VISIBLE);
        menu.setVisibility(View.VISIBLE);
    }

    public void hide_cirlce_menu(){ //Скрыть меню с blur
        menu.setVisibility(View.GONE);
        blurLayout.setVisibility(View.GONE);
    }


}
